import time
from DFRobot_RaspberryPi_DC_Motor import DFRobot_DC_Motor_IIC as Board
from signal import pause


import rospy
from std_msgs.msg import *

board = Board(1, 0x10)
board.set_encoder_enable(board.ALL)
board.set_encoder_reduction_ratio(board.ALL, 120)
board.set_moter_pwm_frequency(1000)
speed = board.get_encoder_speed(board.ALL)

rospy.init_node('WheelEncodersSpeed', anonymous=True)
RWspeed_pub = rospy.Publisher("/wheels/right/speed",Float64, queue_size=10)
LWspeed_pub = rospy.Publisher("/wheels/left/speed",Float64, queue_size=10)


rate = rospy.Rate(100)

while not rospy.is_shutdown():
    speed = board.get_encoder_speed(board.ALL)
    rws = Float64(-speed[0])
    lws = Float64(speed[1])
    RWspeed_pub.publish(rws)
    LWspeed_pub.publish(lws)
    print "LWS", speed[1],"RWS",-speed[0]
    rate.sleep()
