#! /usr/bin/env python

import time
from DFRobot_RaspberryPi_DC_Motor import DFRobot_DC_Motor_IIC as Board
from signal import pause

import rospy
import math 
from nav_msgs.msg import * 
from geometry_msgs.msg import *
import tf_conversions
import math
from std_msgs.msg import *
import tf_conversions 
import tf2_ros 


board = Board(1, 0x10)
board.set_encoder_enable(board.ALL)
board.set_encoder_reduction_ratio(board.ALL, 120)
board.set_moter_pwm_frequency(1000)


def Runge_kutta_Solver(x_prev, y_prev, theta_prev):
  global odom_pub, t_prev
  speed = board.get_encoder_speed(board.ALL)
  RWS = -speed[0]
  LWS = speed[1]
  speed = [RWS,LWS]
  v, thetap = RPM_to_linear(speed)
  ta = rospy.get_time()
  t = ta - t_prev 
  t_prev = ta 

  print "time elapsed" , t 


  k00 = v * math.cos(theta_prev)
  k01 = v * math.sin(theta_prev)
  k02 = thetap

  k10 = v * math.cos(theta_prev + (t/2) * k02)
  k11 = v * math.sin(theta_prev + (t/2) * k02)
  k12 = thetap

  k20 = v * math.cos(theta_prev + (t/2) * k12)
  k21 = v * math.sin(theta_prev + (t/2) * k12)
  k22 = thetap

  k30 = v * math.cos(theta_prev + (t/2) * k22)
  k31 = v * math.sin(theta_prev + (t/2) * k22)
  k32 = thetap

  x = x_prev + (t/6) * (k00 + 2 * (k10 + k20) + k30)
  y = y_prev + (t/6) * (k01 + 2 * (k11 + k21) + k31)
  yaw = (theta_prev + (t/6) * (k02 + 2 * (k12 + k22) + k32)) % (2 * math.pi)

  wheelOdom = Odometry()
  
  xp = v * math.cos(yaw)
  yp = v * math.sin(yaw)
  zp = 0.0

  z = 0.1

  time = rospy.Time.now()

  transform_broscaster(x,y,yaw,time)

  wheelOdom.header.stamp = time
  wheelOdom.header.frame_id = "odom"

  wheelOdom.child_frame_id = "vectornav"

  wheelOdom.pose.pose.position.x = x
  wheelOdom.pose.pose.position.y = y
  wheelOdom.pose.pose.position.z = z

  wheelOdom.pose.pose.orientation = Quaternion(*tf_conversions.transformations.quaternion_from_euler(0.0, 0.0, yaw))

  wheelOdom.twist.twist.linear.x = xp
  wheelOdom.twist.twist.linear.y = yp
  wheelOdom.twist.twist.linear.z = 0.0

  wheelOdom.twist.twist.angular.x = 0.0
  wheelOdom.twist.twist.angular.y = 0.0
  wheelOdom.twist.twist.angular.z = thetap

  odom_pub.publish(wheelOdom)

  return x, y, yaw

def transform_broscaster(x,y,theta,t):

  br = tf2_ros.TransformBroadcaster()
  tr = geometry_msgs.msg.TransformStamped()

  tr.header.stamp = t
  tr.header.frame_id = "odom"
  tr.child_frame_id = "vectornav"
  tr.transform.translation.x = x
  tr.transform.translation.y = y
  tr.transform.translation.z = 0.1
  q = tf_conversions.transformations.quaternion_from_euler(0, 0, theta)
  tr.transform.rotation.x = q[0]
  tr.transform.rotation.y = q[1]
  tr.transform.rotation.z = q[2]
  tr.transform.rotation.w = q[3]

  br.sendTransform(tr)


def RPM_to_linear(speed) :
    global odom_pub

    WB = 0.15
    r = 0.035
    pi = math.pi

    w1 = speed[0] # right wheel 
    w2 = speed[1] # left wheel

    speed1 = (2 * pi * r * w1) / 60 
    speed2 = (2 * pi * r * w2) / 60 

    v = (speed1 + speed2) / 2
    print "v" , v 

    thetap = (speed1 - speed2) / WB
    print "thetap" , thetap

    return v , thetap  


if __name__ == "__main__":

  x_prev = 0.0  
  y_prev = 0.0 
  theta_prev = 0.0

  rospy.init_node('WheelsOdometry', anonymous=True)
  odom_pub = rospy.Publisher("wheels/odom", Odometry, queue_size=10)

  rate = rospy.Rate(100)
  t_prev = rospy.get_time()
  while not rospy.is_shutdown():
    x_prev, y_prev, theta_prev = Runge_kutta_Solver(x_prev, y_prev, theta_prev)
    print "x", x_prev, "y",y_prev, "yaw",theta_prev
    rate.sleep()