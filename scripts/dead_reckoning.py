#! /usr/bin/env python

import rospy
from geometry_msgs.msg import *
from nav_msgs.msg import *
from sensor_msgs.msg import *
from visualization_msgs.msg import * 
import math


def FilteredOdomCallback(msg):
    global start_x, start_y, markerPub, pathPub, path, meterMoved

    odometry = msg
    curr_x = odometry.pose.pose.position.x 
    curr_y = odometry.pose.pose.position.y

    dx = curr_x - start_x
    dy = curr_y - start_y

    print "curr_x" , curr_x , "start_x", start_x
    print "curr_y", curr_y, "start_y", start_y 
     
    distanceMoved = math.sqrt(dx * dx + dy * dy) # Distance moved since last 1 m 
    totalDistance = meterMoved + distanceMoved # Total distance moved since start

    print "Total distance moved: ", totalDistance

    stampedPose = PoseStamped()
    stampedPose.header.frame_id = "/odom"
    stampedPose.header.stamp = rospy.Time.now()
    stampedPose.pose = odometry.pose.pose
    stampedPose.pose.position.z = 0.0

    path.poses.append(stampedPose)
    pathPub.publish(path)

    if (distanceMoved > 1):
        print "Robot Moved 1 meter"
        marker = Marker()
        marker.header.frame_id = "odom"
        marker.header.stamp = rospy.get_rostime()
        marker.id = 0
        marker.type = 2 # sphere
        marker.action = 0

        marker.scale.x = 0.05
        marker.scale.y = 0.05
        marker.scale.z = 0.05

        marker.color.r = 1.0
        marker.color.g = 0.0
        marker.color.b = 0.0
        marker.color.a = 1.0

        marker.lifetime = rospy.Duration(0.0)
        marker.pose = odometry.pose.pose
        marker.pose.position.z = 0.0

        markerPub.publish(marker)
        meterMoved += 1
        start_x = curr_x
        start_y = curr_y



if __name__ == "__main__":

    rospy.init_node('DeadReckoning', anonymous=True)

    start_x = 0.0 
    start_y = 0.0
    meterMoved = 0.0

    path = Path()
    path.header.frame_id = "odom"

    rospy.Subscriber("/odometry/filtered",Odometry , FilteredOdomCallback)
    markerPub = rospy.Publisher("/1mMilestone", Marker, queue_size=2)
    pathPub = rospy.Publisher("/path", Path, queue_size=2)
    rospy.spin()



