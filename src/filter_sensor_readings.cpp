#include "ros/ros.h"
#include "geometry_msgs.h"
#include "sensor_msgs.h"
#include "nav_msgs.h"


void unifiedCallback(const nav_msgs::Odometry::ConstPtr& wheelodom, nav_msgs::Odometry::ConstPtr& topicName)
{
  ROS_INFO("I heard: [%s] on topic \"%s\"", msg->data.c_str(), topicName.c_str());
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "FilterSensors");

  ros::NodeHandle nh;

  ros::Subscriber sub1 = nh.subscribe<nav_msgs::Odometry>("wheels/odom", 1 , boost::bind(&unifiedCallback, _1, "wheels/odom"))
  ros::Subscriber sub2 = nh.subscribe<nav_msgs::Odometry>("vectornav/Odom", 1, boost::bind(&unifiedCallback, _1, "vectornav/Odom"))
  ros::Subscriber sub3 = nh.subscribe<sensor_msgs::Imu>("/mobile_base/sensors/imu_data", 1, boost::bind(&unifiedCallback, _1, "wheels/odom"))

  odomf_pub = ros::Publisher("wheels/odom/filtered",Odometry, queue_size=10)
  imuf_pub = ros::Publisher("/mobile_base/sensors/imu_data/filtered",Imu, queue_size=10)
    
  ros::spin();

  return 0;
}